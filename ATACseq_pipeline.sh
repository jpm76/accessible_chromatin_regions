# bedtools map -a genes.bed -b peaks.bedgraph -c 4 -o mean

bedtools map -a bstricta_genome.20kwindows.bed -b prin123_gwas.bed -c 4 -o max > prin123_20kWindow.txt


# bedtools intersect -a A.bed -b B.bed -c

bedtools intersect -a bstricta_genome.20kwindows.bed -b peak_rangeCheck.bed -c > peak_rangeCheck_20kbWindow.txt

paste peak_rangeCheck_20kbWindow.txt prin123_20kWindow.txt > prin123_peak_rangeCheck_merge.txt


# create a new variable based on specific columns
# recombination measure = 1 recombination/bp

my_file="recomb.WES"

awk '{ if ($4 > 0){ $9="accessible" } else { $9="non-accessible" } print }' OFS='\t' $my_file > $my_file"_result" | l $my_file"_result"

# create a proper bed file and check $13 to make sure 


my_file="recomb.WES"

# remove header

sed '1d' $my_file > $$.tmp && mv $$.tmp $my_file


awk '{ if ($2 > $3){$11=$3; $12=$2; $13=(($2-$3)); $14=(($11-$12)) } else {$11=$2; $12=$3; $13=(($2-$3)); $14=(($11-$12)) } print}' OFS='\t' $my_file > $my_file"_result" | l $my_file"_result"


# find min and max and make sure it makes sense

min=`awk 'BEGIN{a=1000}{if ($14<0+a) a=$14} END{print a}' $my_file"_result"`
max=`awk 'BEGIN{a=-1}{if ($14>0+a) a=$14} END{print a}' $my_file"_result"`
echo "min==" $min
echo "max==" $max


# select the columns that you need (use awk)

awk ' BEGIN { FS="\t" }; {print $1FS$11FS$12FS$7FS$8FS$9FS$10}' recomb.WES_result > recomb.WES_result.bed

# add chr to chromosome #
sed 's/^/chr/g' recomb.WES_result.bed > $$.tmp && mv $$.tmp recomb.WES_result.bed ; head -10 recomb.WES_result.bed

cp recomb.WES_resutl.bed "/Users/juliusmojica/Dropbox (Duke Bio_Ea)/Proj.ATACseq/ATACseq.Data/recomb.WES_resutl.bed"


# find the max recomb in 20kb windows

bedtools map -a bstricta_genome.20kwindows.bed -b recomb.WES_result.bed -c 4 -o max > max.recombWES_mean_20kbwin.txt


# combine negLog10 of prin123, number of peaks, and max recombination_mean

paste prin123_peak_rangeCheck_merge.txt max.recombWES_mean_20kbwin.txt > mean.txt


# get the fold_enrichment value for each location
awk 'BEGIN {FS = "\t"}; {print "chr"$1FS$2FS$3FS$12}' peak_phy.txt | sed '1d' > peak_phy_fold.txt


# check if the start and stop are correct; then check in R using summary

awk '{if ($2 > $3){$5=$3; $6=$2; $7=(($2-$3)); $8=(($4-$5))} else {$5=$2; $6=$3; $7=(($2-$3)); $8=(($4-$5))} print}' OFS='\t' peak_phy_fold.txt > peak_phy_fold_chk.txt; head -10 peak_phy_fold_chk.txt

# get the correct columns 

awk 'BEGIN {FS = "\t"}; {print $1FS$5FS$6FS$4}' peak_phy_fold_chk.txt > $$.tmp && mv $$.tmp peak_phy_fold_chk.txt ; head -10 peak_phy_fold_chk.txt


#####################################################################
# MAX FOLD ENRICHMENT 
#####################################################################


# find max fold enrichment within 20kb windows with the mean.txt data

bedtools map -a bstricta_genome.20kwindows.bed -b peak_phy_fold_chk.txt -c 4 -o max > max_fold_20kbwindows.bed

# combine number of peaks, neg_log10 of prin123, recombination rate per 1bp, and sequence fold enrichment in a single file

paste mean.txt max_fold_20kbwindows.bed > peak_neglog_recomb_foldenrich.txt ; head -10 peak_neglog_recomb_foldenrich.txt

# add column headers

echo -e 'chr\tstart\tend\tpeakNum\tchr1\tstart1\tend1\tneglog10_prin123\tchr2\tstart2\tend2\trecombination_rate\tchr3\tstart3\tend3\tenrichment' > header;
cat header peak_neglog_recomb_foldenrich.txt > peak_neglog_recomb_folderich_header.txt;
head -2 peak_neglog_recomb_folderich_header.txt


# find the max recomb in 20kb windows

bedtools map -a bstricta_genome.20kwindows.bed -b recomb.WES_result.bed -c 4 -o mean > mean.recombWES_mean_20kbwin.txt


# combine negLog10 of prin123, number of peaks, and max recombination_mean

paste prin123_peak_rangeCheck_merge.txt max.recombWES_mean_20kbwin.txt > mean.txt


# get the fold_enrichment value for each location
awk 'BEGIN {FS = "\t"}; {print "chr"$1FS$2FS$3FS$12}' peak_phy.txt | sed '1d' > peak_phy_fold.txt

# check if the start and stop are correct; then check in R using summary

awk '{if ($2 > $3){$5=$3; $6=$2; $7=(($2-$3)); $8=(($4-$5))} else {$5=$2; $6=$3; $7=(($2-$3)); $8=(($4-$5))} print}' OFS='\t' peak_phy_fold.txt > peak_phy_fold_chk.txt; head -10 peak_phy_fold_chk.txt

# get the correct columns 

awk 'BEGIN {FS = "\t"}; {print $1FS$5FS$6FS$4}' peak_phy_fold_chk.txt > $$.tmp && mv $$.tmp peak_phy_fold_chk.txt ; head -10 peak_phy_fold_chk.txt


#####################################################################
# MEAN FOLD ENRICHMENT 
#####################################################################

# find mean fold enrichment within 20kb windows with the mean.txt data

bedtools map -a bstricta_genome.20kwindows.bed -b peak_phy_fold_chk.txt -c 4 -o mean > mean_fold_20kbwindows.bed

# combine number of peaks, neg_log10 of prin123, recombination rate per 1bp, and sequence fold enrichment in a single file

paste mean.txt mean_fold_20kbwindows.bed > peak_neglog_recomb_meanfoldenrich.txt ; head -10 peak_neglog_recomb_meanfoldenrich.txt

# calculate scaled_enrichment = meanfoldenrich * peakNum

awk '{{$17=(($4*$16))} print}' OFS='\t' peak_neglog_recomb_meanfoldenrich.txt > $$.tmp && mv $$.tmp peak_neglog_recomb_meanfoldenrich.txt;
head -10 peak_neglog_recomb_meanfoldenrich.txt


# ACR type

awk '{if ($17>0) {$18="accessible"} else {$18="non-accessible"} print}' OFS='\t' peak_neglog_recomb_meanfoldenrich.txt > $$.tmp && mv $$.tmp peak_neglog_recomb_meanfoldenrich.txt;
head -10 peak_neglog_recomb_meanfoldenrich.txt


# hit vs non-hit

awk '{if ($8>8) {$19= "gwas-hit"} else {$19 = "non-gwas-hit"} print}' OFS='\t' peak_neglog_recomb_meanfoldenrich.txt > $$.tmp && mv $$.tmp peak_neglog_recomb_meanfoldenrich.txt;
head -10 peak_neglog_recomb_meanfoldenrich.txt

#add header with acr type

echo -e 'chr\tstart\tend\tpeakNum\tchr1\tstart1\tend1\tneglog10_prin123\tchr2\tstart2\tend2\trecombination_rate\tchr3\tstart3\tend3\tmean_enrichment\tscaled_enrichment\tacr_type\tgwasType' > header;
cat header peak_neglog_recomb_meanfoldenrich.txt > peak_neglog_recomb_meanfolderich_acrType_hit_header.txt;
head -2 peak_neglog_recomb_meanfolderich_acrType_hit_header.txt

# perform Fisher's between GS_prin123 and ACR
bedtools fisher -a peak_rangeCheck_20kbWindow_withPeaks.bed -b prin123_20kWindow_sigOnly.bed -g bstricta_genome.txt

bedtools fisher -b peak_rangeCheck_20kbWindow_withPeaks.bed -a prin123_20kWindow_sigOnly.bed -g bstricta_genome.txt


